import React from 'react';
import ReactDOM from 'react-dom';
import configure from './store';
import App from './App';

const store = configure();

ReactDOM.render(<App store={store} />, document.getElementById('app'));
