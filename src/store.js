import { createStore, createReducer } from 'redux';

export const types = {
  ADD_NOTE: 'ADD_NOTE',
  COMPLETE_NOTE: 'COMPLETE_NOTE',
  SHOW_ALERT: 'SHOW_ALERT',
  HIDE_ALERT: 'HIDE_ALERT',
};

// Actions
export const addNote = (label) => ({ type: types.ADD_NOTE, payload: { label } });
export const completeNote = (idx) => ({ type: types.COMPLETE_NOTE, payload: { idx } });
export const showAlert = (label) =>  ({ type: types.SHOW_ALERT, payload: { label } });
export const hideAlert = () => ({ type: types.HIDE_ALERT });

const initialState = {
  notes: [
    { label: 'Active note', isActive: true, timeout: 3 },
    { label: 'Completed note', isActive: false, timeout: 3 },
  ],
  alert: null
};

// reducers
const handlers = {
  [types.ADD_NOTE]: (state, { label }) => ({
    ...state,
    notes: [...state.notes, { label, isActive: true, timeout: 3 }],
  }),
  [types.COMPLETE_NOTE]: (state, { idx }) => ({
    ...state,
    notes: [...state.notes.slice(0, idx), { ...state.notes[idx], isActive: false }, ...state.notes.slice(idx + 1)],
  }),
  [types.SHOW_ALERT]: (state, { label }) => ({
    ...state,
    alert: label,
  }),
  [types.HIDE_ALERT]: (state) => ({
      ...state,
      alert: null,
  }),
}

const reducer = (state = initialState, action) => {
  const handler = handlers[action.type];
  return handler ? handler(state, action.payload, action.meta) : state;
};

export default () => createStore(
  reducer,
  initialState
);
