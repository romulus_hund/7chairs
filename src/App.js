import React, { useState } from 'react';
import { connect } from 'react-redux';
import {addNote, completeNote, hideAlert, showAlert} from './store';

function App({ notes, alert, addNote, completeNote, showAlert, hideAlert }) {
  const [note, setNote] = useState('');

  const clearAlert = (i) => {
    setTimeout(() => {
      hideAlert()
      completeNote(i)
    }, 5000)
  };

  const handleOnClick = (n, i) => {
    setTimeout(() => {
      showAlert(n.label)
      clearAlert(i)
    }, n.timeout * 1000)
  };

  return (<>
    <h1>TODO APP</h1>
    <h2>Alert [ {alert} ]</h2>
    <ul>
      {notes.map((n, i) => (
        <li onClick={() => handleOnClick(n, i)} key={i}>
          {n.isActive ? n.label : <del>{n.label}</del>}
        </li>

      ))}
    </ul>
    <input value={note} onChange={e => setNote(e.target.value)} />
    <button onClick={() => addNote(note)}>Add</button>
  </>
  );
}

const stateToProps = ({ notes, alert }) => ({
  notes,
  alert
});

export default connect(stateToProps, { addNote, completeNote, showAlert, hideAlert })(App);